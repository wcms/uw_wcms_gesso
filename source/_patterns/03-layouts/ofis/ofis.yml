---
ofis_data:
#  Next 5 fields are dynamic, modified by the controller.
  title: 'Jane Doe'
  faculty_title: 'Dean, Faculty of Engineering / Professor, Mechanical and Mechatronics Engineering'
  name: 'Jane Doe'
  picture: '../../../source/images/ofis/ofis_person.png'
  biography: 'Jane Doe, PhD, PEng is currently Dean of the Faculty of Engineering at the University of Waterloo and is the ninth dean since the Faculty was founded in 1957. She was previously Dean of the College of Engineering and Physical Sciences at the University of Guelph (2017 to 2020).</p><p>Prior to her time in Guelph, Wells was a professor of mechanical and mechatronics engineering at Waterloo for 10 years. She received awards for graduate supervision from both the Faculty and the University in 2017.</p><p>An accomplished materials engineer, Wells also served as the Associate Dean of Outreach for Waterloo Engineering between 2008 and 2017, and chaired its Women in Engineering committee for many years. She chaired the Ontario Network of Women in Engineering from 2013 to 2018.</p><p>Wells began her academic career as a professor in materials engineering at the University of British Columbia from 1996 to 2007, and has worked in the steel industry in Canada and internationally.</p><p>The co-author of two books including one on Canadian women innovators and the second on Canadian women in materials, her research focuses on the relationship between processing, structure and properties for advanced metallic alloys used in the transportation sector.</p><p>Wells is not currently accepting applications to supervise new graduate students.'
  personal:
    last_name: 'Jane'
    first_name: 'Doe'
    middle_name: ''
    preferred_name: ''
    home_dept: 'Mechanical & Mechatronics Engineering'
    group_type: 'FAC'
    rank: 'Adjunct Professor'
    title: 'Dean of Engineering||Dean, Engineering'
    userid: 'engdean'
    accepting_grad_students: false
    adds_status: false
    offices: 'E7 1234, E5 2345'
    phones: '519-888-1234 x12345, 519-888-1234 x23456'
    fax: '519-888-4321'
    research_interests: 'Main research interests are in text information retrieval (IR), concerned with the organization and access to unstructured textual information. My first stream of research deals with developing new methods of retrieving documents based on statistical and linguistic text processing techniques.'
    brief_research_interests: 'Information retrieval; natural language processing; information extraction; artificial intelligence; machine learning'
    bio: 'Jane Doe, PhD, PEng is currently Dean of the Faculty of Engineering at the University of Waterloo and is the ninth dean since the Faculty was founded in 1957. She was previously Dean of the College of Engineering and Physical Sciences at the University of Guelph (2017 to 2020).\r\n\r\nPrior to her time in Guelph, Wells was a professor of mechanical and mechatronics engineering at Waterloo for 10 years. She received awards for graduate supervision from both the Faculty and the University in 2017.\r\n\r\nAn accomplished materials engineer, Wells also served as the Associate Dean of Outreach for Waterloo Engineering between 2008 and 2017, and chaired its Women in Engineering committee for many years. She chaired the Ontario Network of Women in Engineering from 2013 to 2018.\r\n\r\nWells began her academic career as a professor in materials engineering at the University of British Columbia from 1996 to 2007, and has worked in the steel industry in Canada and internationally.\r\n\r\nThe co-author of two books including one on Canadian women innovators and the second on Canadian women in materials, her research focuses on the relationship between processing, structure and properties for advanced metallic alloys used in the transportation sector.\r\n\r\nWells is not currently accepting applications to supervise new graduate students.'
    picture: 'janedoe.png'
    profile_job_title: 'Dean, Faculty of Engineering / Professor, Mechanical and Mechatronics Engineering'
    institution: 'McGill University'
    degree_name: "Bachelor's"
    expertise_keywords:
      - "Process modelling"
      - "Hot deformation of metallic materials"
      - "Casting of light metals"
      - "Boiling water heat transfer"
      - "Microstructure/Processing Models"
      - "Automotive"
      - "Digital Factories"
      - "Lightweight Materials"
      - "Structural Crashworthiness"
      - "Additive Manufacturing"
    webpages:
      - '[Jane Doe] (https://uwaterloo.ca)'
    profile_phones: ''
    webpages_formatted:
      - '<a href="https://uwaterloo.ca">Jane Doe</a>'
    email: 'engdean@uwaterloo.ca'
  research_interests:
    - sresearch_interest: 'Information retrieval; natural language processing; information extraction; artificial intelligence; machine learning'
  courses:
    - subject: 'ME'
      num: 340
      title: 'Manufacturing Processes<ul><li>Taught in 2016, 2017</li></ul>'
    - subject: 'ME'
      num: '738'
      title: 'Special Topics in Materials<ul><li>Taught in 2017</li></ul>'
  news:
    - article_title: 'Honouring the December 6 victims'
      url: 'https://uwaterloo.ca/news/engineering/honouring-december-6-victims'
    - article_title: 'December 6 attacker could not hold women back'
      url: 'https://uwaterloo.ca/news/engineering/december-6-attacker-could-not-hold-women-back'
    - article_title: 'New dean named for Waterloo Engineering'
      url: 'https://uwaterloo.ca/stories/eng-news-new-dean-named-waterloo-engineering'
    - article_title: "Engineers are 'hidden heroes' in COVID-19 battle"
      url: 'https://uwaterloo.ca/stories/engineering/engineers-are-hidden-heroes-covid-19-battle'
  contribution:
    - type: 'Refereed Journal Paper'
      value: 'Wei, Guo-bing and Peng, Xiao-dong and Hu, Fa-ping and Hadadzadeh, Amir and Yan, YANG and Xie, Wei-dong and Wells, Mary A, Deformation behavior and constitutive model for dual-phase Mg--Li alloy at elevated temperatures, Transactions of Nonferrous Metals Society of China, 26(2), 2016, 508 - 518'
    - type: 'Refereed Journal Paper'
      value: 'Wei, Guobing and Mahmoodkhani, Yahya and Peng, Xiaodong and Hadadzadeh, Amir and Xu, Tiancai and Liu, Junwei and Xie, Weidong and Wells, Mary A, Microstructure evolution and simulation study of a duplex Mg--Li alloy during Double Change Channel Angular Pressing, Materials & Design, 90, 2016, 266 - 275'
    - type: 'Refereed Journal Paper'
      value: 'Mahmoodkhani, Yahya and Wells, Mary A, Co-extrusion process to produce Al--Mg eutectic clad magnesium products at elevated temperatures, Journal of Materials Processing Technology, 232, 2016, 175 - 183'
    - type: 'Refereed Journal Paper'
      value: 'Hadadzadeh, Amir and Wells, Mary A and Javaid, Amjad, Warm and Hot Deformation Behavior of As-Cast ZEK100 Magnesium Alloy, Experimental Mechanics, 56(2), 2016, 259 - 271'
    - type: 'Refereed Journal Paper'
      value: 'Di Ciano, Massimo and Weckman, DC and Wells, MA, Development of an Analog System to Simulate Interface Formation During Fusion™ Casting, Metallurgical and Materials Transactions B, 47(2), 2016, 905 - 919'
  education:
    - institution: 'University of British Columbia'
      country: 'Canada'
      degree_yr: '1996'
      degree_mon_nbr: '6'
      name_of_discipline: 'Metals and Materials Engineering'
      degree_name: 'Doctorate'
    - institution: 'McGill University'
      country: 'Canada'
      degree_yr: '1987'
      degree_mon_nbr: '6'
      name_of_discipline: 'Metallurgical Engineering'
      degree_name: "Bachelor's"
