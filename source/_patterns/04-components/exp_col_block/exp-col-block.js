/**
 * @file
 */

(function ($, Drupal) {
  Drupal.behaviors.expcol = {
    attach: function (context, settings) {
      $(document).ready(function () {

        $('.uw-exp-col').each(function () {

          // The id selector for the exp/col.
          var id_selector = '#' + $(this).attr('id');

          // Open all the details for the clicked E/C.
          $(id_selector + ' button[data-type="expand-all"]').click(function () {
            $(id_selector + ' details').each(function () {
              $(this).attr('open', 'TRUE');
            });
          });

          // Close all the details for the clicked E/C.
          $(id_selector + ' button[data-type="collapse-all"]').click(function () {
            $(id_selector + ' details').each(function () {
              $(this).removeAttr('open');
            });
          });
        });
      });
    }
  };
})(jQuery, Drupal);
