/**
 * Javascript for facts and figures
 */

(function ($, Drupal) {
    Drupal.behaviors.factfigure = {
        attach: function (context, settings) {
            function runCarousel(id, numOfItems)
            {
                // Add the carousel to the FF using the id.
                $(id + " .owl-carousel").owlCarousel(
                    {
                        margin: 10,
                        nav: true,
                        navContainerClass: "uw-owl-nav",
                        navText: [
                        "‹ prev",
                        "next ›"
                        ],
                        responsiveClass: true,
                        responsive: {
                              0: {
                                    items: 1
                            },
                              600: {
                                    items: numOfItems <= 2 ? (numOfItems - 1 > 0) ? numOfItems - 1 : 1 : 2

                            },
                              1000: {
                                    items: numOfItems

                            }
                        }
                    }
                );
            }
            // Check if the element is is in view.
            function isElementInViewport(elem)
            {
                var $elem = $(elem);
                // Get the scroll position of the page.
                var viewportTop = $("html").scrollTop();
                var viewportBottom = viewportTop + $(window).height();

                // Get the position of the element on the page.
                // Adds a little padding so it triggers when the
                // element is closer to the middle of the page.
                var elemTop = Math.round($elem.offset().top);
                var elemBottom = elemTop + $elem.height();
                return ((elemTop < viewportBottom) && (elemBottom > viewportTop));
            }
            // Run the animation if the element is in view.
            function checkAnimation(infoHorizontal, infoVertical, infoNumber)
            {
                $(infoHorizontal).each(
                    function () {
                        if ($(this).hasClass("animated")) {
                            return;
                        }
                        if (isElementInViewport($(this))) {
                            runAnimation($(this));
                        }
                    }
                );
                $(infoVertical).each(
                    function () {
                        if ($(this).hasClass("animated")) {
                            return;
                        }
                        if (isElementInViewport($(this))) {
                            runAnimation($(this));
                        }
                    }
                );
                $(infoNumber).each(
                    function () {
                        if ($(this).hasClass("animated")) {

                            return;
                        }
                        if (isElementInViewport($(this))) {
                            runAnimation($(this));
                        }
                    }
                );
            }
            // Isolating this to try to run it when the slider changes.
            function runAnimation(el)
            {
                var type = el.data("infographic-type");
                var percent = el.data("percent");

                // Animate the graph.
                if (type === "horizontal") {
                    el.find(".graph-wrapper .graph").animate({"width" : percent + "%"}, 2000);
                }
                else if (type === "vertical") {
                    el.find(".graph-wrapper .graph").animate({"height" : percent + "%"}, 2000);
                }
                animateNumber(el.find(".timer"), percent);
                el.addClass("animated");
            }
            // Animate the number.
            function animateNumber(el, percent)
            {
                var hasComma;
                var num;
                // Find out if the number has a thousand marker (comma).
                if (percent.toString().indexOf(",") >= 0) {
                    hasComma = true;
                    // If so, remove it for now so the animation will work.
                    percent = percent.replace(/,/g, "");
                }
                else {
                    hasComma = false;
                }

                $({counter: 0}).animate(
                    {counter: percent}, {
                        duration: 2000,
                        step: function () {
                            var num = Math.ceil(this.counter).toString();
                            if (hasComma) {
                                // Add back the comma.
                                while (/(\d+)(\d{3})/.test(num)) {
                                    num = num.replace(/(\d+)(\d{3})/, "$1" + "," + "$2");
                                }
                            }
                            $(el).text(num);
                        },
                        // Ensure the correct value is shown when animation is complete
                        // fixes issue with incomplete counters for large numbers
                        // see: https://stackoverflow.com/questions/50331552/jquery-
                        // counter-fails-to-show-the-correct-value-after-animation
                        complete : function () {
                            if (hasComma) {
                                // Add back the comma.
                                percent = num.replace(/(\d+)(\d{3})/, "$1" + "," + "$2");
                            }
                            $(el).text(percent);
                        }
                    }
                );
            }
            // Build Info graphics.
            function setupInfoGraphics(id)
            {

                // For circle infographics, first
                // check to make sure library is loaded.
                if ($.fn.circliful) {

                      // Set default options for circle infographics.
                      var $circlifulOptions = "";
                      $circlifulOptions = {
                            animation: 1,
                            animationStep: 3,
                            foregroundBorderWidth: 15,
                            backgroundBorderWidth: 15,
                            backgroundColor: "#a2a2a2",
                            animateInView: "true",
                            fontColor: "#4e4e4e",
                            percentageTextSize: "35",
                    };

                      var circles = id + " .infographic-circle";

                      // Create circle infographic.
                    $(circles).each(
                        function () {
                            $(this).circliful($circlifulOptions);
                        }
                    );

                      var halfCircles = id + " .infographic-half-circle";

                      // Create half circle.
                    $(halfCircles).each(
                        function () {
                            $(this).circliful(
                                $.extend($circlifulOptions, {halfCircle: "true"})
                            );
                        }
                    );
                }

                // Setting selectors for the animation checks
                // and bar builds.
                var infoNumber = id + " .infographic-number";
                var infoVertical = id + " .infographic-vertical";
                var infoHorizontal = id + " .infographic-horizontal";

                $(infoHorizontal).each(
                    function () {
                        $(this).append("<div class='graph-wrapper'><span class='graph'></span></div><span class='timer'>" + $(this).data("percent") + "</span>");
                    }
                );

                $(infoVertical).each(
                    function () {
                        $(this).append("<div class='graph-wrapper'><span class='graph'></span></div><span class='timer'>" + $(this).data("percent") + "</span>");

                    }
                );

                // Check when the page loads.
                checkAnimation(infoHorizontal, infoVertical, infoNumber);

                // Capture scroll events.
                $(window).scroll(
                    function () {
                        checkAnimation(infoHorizontal, infoVertical, infoNumber);
                    }
                );
            };
            // Function to run the FF code.
            function runFF()
            {

                // Step through each FF on the page.
                $(".uw-ff").each(
                    function () {
                        // Get the id to reference the individual FF.
                        // Need this to ensure that if more than one FF on the page,
                        // that all FFs get the carousel added.
                        var id = "#uw-ff-" + $(this).data("id");

                        // Check to see if we have a carousel
                        // set from drupal in twig.
                        var hasCarousel = $(this).data("carousel") === "yes";

                        // If we have one.
                        if (hasCarousel) {

                            // Get the number of items for the carousel, if any.
                            var numOfItems = $(id).data("num-per-carousel") !== "" ? $(this).data("num-per-carousel") : 3;

                            // Run the carousel.
                            runCarousel(id, numOfItems);
                        }
                        // Check to see if info graphic is animated
                        // return else run setup. ensures we dont fire 2x.
                        if ($(".highlighted-fact-infographic ").hasClass("animated")) {
                            return;
                        }
                        else {
                            setupInfoGraphics(id);
                        }
                    }
                );
            }
            // When the data-ff element is on page we know the update button is on page.
            if ($("[data-layout-builder-target-highlight-id] [data-ff='yes']").length) {
                if ($("[data-ff='yes']")) {
                    runFF();
                }
            }
            $(document).ready(
                function () {
                    // Run the FF code on each of the figures.
                    if ($(".uw-ff").length) {
                        runFF();
                    }
                }
            );
        }
    };
})(jQuery, Drupal);
