/**
 * @file
 */

(function ($, Drupal) {
  Drupal.behaviors.imagegallery = {
    attach: function (context, settings) {

      $(document).ready(function () {

        // Step through each FF on the page.
        $('.uw-ig').each(function () {

          // Get the id to reference the individual FF.
          // Need this to ensure that if more than one FF on the page,
          // that all FFs get the carousel added.
          var id = '#uw-ig-' + $(this).data('id');
          var imagesNum = $(this).data('images-num') || 1;
          var navStyle = $(this).data('nav') || 'both';

          // Create owl carouse config first.
          var carousel = {
            loop: true,
            dots: true,
            nav: true,
            navContainerClass: 'uw-owl-nav',
            navText:[
              'prev',
              'next'
            ],
            margin: 10,
            responsiveClass: true,
            responsive: {
              0: {
                items: 1
              },
              600: {
                items: imagesNum
              },
              1000: {
                items: imagesNum
              }
            }
          };

          if (navStyle === 'pagination') {
            carousel.nav = false;
          }
          else if (navStyle === 'navigation') {
            carousel.dots = false;
          }

          // Add the carousel to the FF using the id.
          $(id + ' .owl-carousel').owlCarousel(carousel);

          // Lightbox enchancements
          $('.uw-lightbox__open').on('click', function () {
            $(id + ' .uw-lightbox').addClass('openLightBox');
            $('html').addClass('no-scroll');

          });
          // Lightbox close
          $(id + ' .uw-lightbox__close').on('click', function () {
              $('.uw-lightbox').removeClass('openLightBox');
              $('html').removeClass('no-scroll');
          });
          // If next is clicked
          $(id + ' .uw-lightbox__next').on('click', function () {
            if (!$(id + ' .uw-lightbox').hasClass('openLightBox')){
              $(id + ' .uw-lightbox').addClass('openLightBox');
            }
          });
          // If prev is clicked
          $(id + ' .uw-lightbox__prev').on('click', function () {
            if (!$(id + ' .uw-lightbox').hasClass('openLightBox')){
                $(id + ' .uw-lightbox').addClass('openLightBox');
            }
          });

          // Faking a click for esc
          function fakeClick() {
            //use url to build the fake anchor id
            var url = window.location.href;
            //Regex to replace the text "lightbox" with "ig" and trim last "-###"
            var galleryAnchor = url.substring(url.lastIndexOf('/') + 1).replace( /(?:^|\W)lightbox(?:$|\W)/, "-ig-").replace(/\-\d+$/, "");
            // Create the fake element
            var escFake = document.createElement('a');

            var linkText = document.createTextNode("fake click ");

            escFake.appendChild(linkText);
            escFake.title = "my title text";
            escFake.href = galleryAnchor;
            escFake.classList = "uw-lightbox__close off-screen";

            // Append the fake button
            document.body.appendChild(escFake);
            //Click the button
            escFake.click();
            // Remove no scroll
            $('html').removeClass('no-scroll');
            // Remove open class
            $('.uw-lightbox').removeClass('openLightBox');
            // Remove the fake button
            document.body.removeChild(escFake);

          }
          // Attach the keyup event to Escape tp close
          $(document).on('keyup', function (evt) {
            if (evt.keyCode == 27) {
              fakeClick();
            }
          });
          // If click in outside lightbox div then close
          $(document).click( function (evt) {
            if ($(evt.target).is( $('.uw-lightbox.openLightBox'))) {
              fakeClick();
            }
          });
        });
      });
    }
  };
})(jQuery, Drupal);
