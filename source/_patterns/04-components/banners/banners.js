/**
 * @file
 */

(function ($, Drupal) {
  Drupal.behaviors.cardbanner = {
    attach: function (context, settings) {
      $(document).ready(function () {

        // Step through each Banners on the page.
        $('.uw-carousel__banner').each(function () {

          var selector = 'div[data-uuid="' + $(this).attr("data-uuid") + '"] .owl-carousel';
          // Get  banner carousel.
          var owl = $(selector);
          // Get the number of items for the carousel, if any.
          // For banners we are only ever showing one item at
          // a time (for reference Facts & Figures show more).
          var num_of_items = 1;

          var banner_slide_speed = $(this).attr('data-slide-speed');

          // The flag for autoplay.
          var banner_autoplay = true;

          // If there is no autoplay, or we are in layout builder, set flag to false.
          if ($(this).attr('data-autoplay') != 1 || $('.layout-builder').length) {
            banner_autoplay = false;
          }

          // Used to hide buttons when single banner
          if (owl.children().length <= 1) {
            $(this).addClass('banner-single');
          }

          // Get the loop value.
          // Owl carousel setting to loop back to beginning.
          var banner_loop = true;

          // Get dots.
          // Owl carousel setting show dots.
          // If banner has more than 1 slide show dots.
          var banner_dots = false;
          if (owl.children().length > 1) {
             banner_dots = true;
          }
          // Get nav.
          // Owl carousel setting show prev next.
          var banner_nav = false;
          if ($(this).hasClass('uw-carousel__banner-inset')){
              banner_nav = true;
          }
            // Get the play and stop buttons.
          var banner_play = $(this).find($('.uw-play'));
          var banner_pause = $(this).find($('.uw-pause'));

          // Actions for play button.
          banner_play.on('click',function () {
            owl.trigger('play.owl.autoplay', [banner_slide_speed]);
            banner_play.css('display', 'none');
            banner_pause.css('display', 'block');
          });

          // Actions for pause button.
          banner_pause.on('click',function () {
            owl.trigger('stop.owl.autoplay');
            banner_pause.css('display', 'none');
            banner_play.css('display', 'block');
          });

          // Add the carousel to the banner using the id.
          owl.owlCarousel({
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            autoplay: banner_autoplay,
            autoHeight : true,
            autoplayTimeout: banner_slide_speed,
            autoplayHoverPause: true,
            dots: banner_dots,
            dotsClass: 'uw-owl-nav__dots',
            loop: banner_loop,
            nav: banner_nav,
            navContainerClass: 'uw-owl-nav__prevnext',
            navText:[
                '',
                ''
            ],
            responsiveClass: true,
            responsive: {
              0: {
                items: 1
              },
              600: {
                items: num_of_items <= 2 ? (num_of_items - 1 > 0) ? num_of_items - 1 : 1 : 2

              },
              1000: {
                items: num_of_items

              }
            },
          });
        }).on('click', '.owl-dot', function () {
          // Pause the slideshow if they click to advance to a specific slide.
          // We don't need to check if this is present because jQuery will just ignore this if it isn't.
          $(this).closest('.uw-carousel__banner').find('.uw-pause').click();
        });
      });
    }
  };
})(jQuery, Drupal);
