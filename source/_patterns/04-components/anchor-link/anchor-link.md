---
el: .uw-anchor
title: Anchor link
---

__Variables:__
* anchor_link: [array] Variables for anchor link.
    * anchor: [string] Anchor element.
    * anchor_url: [string] Current page url for anchor element.
