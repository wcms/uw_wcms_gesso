---
el: .uw-mailchimp
title: Mailchimp
---

__Variables:__
* mailchimp: [array] Variables for mailchimp.
    * sourcecode: [string] Mailchimp sourcecode.
    * uniqueid: [string] Unique id of the mailchimp component.
