/**
 * @file
 */

(function ($) {
  Drupal.behaviors.responsive_menu_combined = {
    attach: function (context, settings) {
      'use strict';

      // Close "other users" by default.
      $('.uw_other h3 span:first-child').html('&#9656;');
      $('.uw_other ul').hide();
      // Since we're making the headers visible, wrap them in a button.
      $('.uw-whos-online-block h3').wrapInner('<button></button>');
      // Add appropriate ARIA attributes for default states.
      $('.uw_privileged button').attr('aria-expanded', 'true');
      $('.uw_other button').attr('aria-expanded', 'false');
      // Handle show/hide.
      $('.uw-whos-online-block button').on('click', function () {
        let $list = $(this).closest('div').find('ul');
        if ($list.is(":visible")) {
          $list.hide();
          $('span:first-child',this).html('&#9656;');
          $(this).attr('aria-expanded', 'false');
        }
else {
          $list.show();
          $('span:first-child',this).html('&#9662;');
          $(this).attr('aria-expanded', 'true');
        }
      });
    }
  };
})(jQuery);
