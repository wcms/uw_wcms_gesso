---
el: .whos-online
title: Who's online
---

__Variables:__
* privileged [list] List of users with privileged roles.
* others [list] List of users with all other roles.
