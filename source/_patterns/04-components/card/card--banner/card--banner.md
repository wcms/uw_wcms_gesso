---
el: .card--banner
title: Card banner
---
__Variables:__
* banners: [array] List of banners and associated settings.
    * images [array]
        * sources [array] List of source sets for picture element.
        * img_element [string] URL to image fallback.
        * alt [string]
        * big_text [string] Text to be used as the title.
        * small_text [string] Text to be used as the sub-title.
        * link [string] URL to be used for the image,
        
    * autoplay [integer] 1 or 0 to turn on autoplay of banners.
    * slide_speed [integer] The speed for the transition of banners.
    * style [string] The style to be used for the banners.
    * transition_speed [integer] The speed of the transitions between banners.
