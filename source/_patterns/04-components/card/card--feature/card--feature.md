---
el: .card--feature
title: Feature Card
---

__Variables:__
* modifier_classes: [string] Classes to modify the default component styling.
* title: [string] Title of the featured card.
* url: [string] URL of the card.
* date: [string] Date of the card.
* read_more: [boolean] Whether to show the read more link.
* footer: [string] Footer content of the card.
* sources: [string] URL for the srcset of picture tag inside responsive-image.
* img_element: [string] Url for the fallback image.
* alt: [string] Alt text to the image used in the picture tag or fallback image.
* content: [string] Content of the card.
