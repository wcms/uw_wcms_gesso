---
el: .card
title: Card
---

__Variables:__
* modifier_classes: [string] Classes to modify the default component styling.
* title: [string] Title of the card.
* header_level: [string] Set the level of herder <h?> tag for card
* url: [string] URL of the card.
* author_name:  [string] Author name.
* author_link:  [string] URL for the author link.
* show_hover: [boolean] to show outline and shadow on hover
* date: [string] Date of the card.
* date_format: [string] Choose the date format used for presentation.
    * long-date
    * long-datetime
    * medium-date
    * medium-datetime
    * medium-date-short-month
    * short-date
    * short-datetime
* read_more: [boolean] Whether to show the read more link.
* footer: [string] Footer content of the card.
* sources: [string] URL for the srcset of picture tag inside responsive-image.
* img_element: [string] Url for the fallback image.
* alt: [string] Alt text to the image used in the picture tag or fallback image.
* content: [string] Content of the card.
* social_media: [array] Social media icons that can be included to card
    * text
    * url
* social_media_placement: [string] Class name for where social media is placed"
* tags: [array] List of taxonomy tags
    * type: [string] Type of the tag, it will add class ".tag--type".
    * size: [string] Size of the tag, it will add class ".tag--size"
    * url: [string] URL of the tag.
    * title: [string] Title of the tag.
