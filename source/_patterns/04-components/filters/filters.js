/**
 * @file
 */

(function ($, Drupal) {
  Drupal.behaviors.filtersopen = {
    attach: function (context, settings) {
      $(document).ready(function () {
        $('.view-filters details').each(function () {
          $(this).attr("open", "");
        });
      });
    }
  };
})(jQuery, Drupal);
