---
el: .uw-blockquote
title: Blockquote
---

__Variables:__
* blockquote: [array] Data for the blockquote
    * text: [string] Text for the quote.
    * attribution: [string] Attribution for the quote.
