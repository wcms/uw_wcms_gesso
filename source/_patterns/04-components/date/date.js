(function ($, Drupal) {
    Drupal.behaviors.uwdate = {
        attach: function (context, settings) {
            $(document).ready(
                function () {

                    $('.uw-date-details').each(
                        function () {

                            var findHeight = $(this).find($('.uw-date-details__summary .uw-date'));
                            var dateHeight = findHeight.outerHeight(true) + 'px';

                            // On click set the height of parent card__date.
                            // allow for no jump when 2 lines

                            $(this) .on(
                                'click', function () {
                                    // Get the parent and set min-height.
                                    $(this).parent('.card__date').css('min-height', dateHeight);

                                }
                            );
                        }
                    );
                }
            );
        }
    };
})(jQuery, Drupal);
