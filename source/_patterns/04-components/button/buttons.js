/**
 * Javascript for buttons
 */

 (function ($, Drupal) {
    Drupal.behaviors.buttons = {
        attach: function (context, settings) {
            // Only show the show/hide button if the user is logged in and on the "view", "layout", or "latest version" tabs.
            // (We have picked elements that should only exist on those pages, and only when the user has editing access.)
            $(document).ready(
                function () {
                    if ($('.user-logged-in .uw-content-moderation, .user-logged-in #edit-moderation-state-wrapper, .user-logged-in #block-tabs a.is-active[href$=\'latest\']').length) {
                        $('.uw-show-hide').show().on(
                            'click', function () {
                                toggleTabs();
                            }
                        );
                    }
                    function toggleTabs()
                    {

                        var x = document.getElementById('block-tabs');
                        var y = document.getElementsByClassName('uw-content-moderation')[0];
                        var z = document.getElementById('edit-moderation-state-wrapper');

                        if (x.style.display === 'none') {
                            x.style.display = 'block';
                        } else {
                            x.style.display = 'none';
                        }
                        if (y) {
                            if (y.style.display === 'none') {
                                y.style.display = 'block';
                            } else {
                                y.style.display = 'none';
                            }
                        }
                        if (z) {
                            if (z.style.display === 'none') {
                                z.style.display = 'block';
                            } else {
                                z.style.display = 'none';
                            }
                        }
                    }
                }
            );
        }
        };
 })(jQuery, Drupal);
