/**
 * @file
 */

(function ($, Drupal) {
  Drupal.behaviors.uwexpcolall = {
    attach: function (context, settings) {
      $(document).ready(function () {

        $('.uw-exp-col-expand-all').click(function () {
          var uuid = $(this).attr('data-uuid');
          $('[data-uuid="' + uuid + '"] details').each(function () {
            $(this).attr("open", "");
          });
        });

        $('.uw-exp-col-collapse-all').click(function () {
          var uuid = $(this).attr('data-uuid');
          $('[data-uuid="' + uuid + '"] details').each(function () {
            $(this).removeAttr("open");
          });
        });

      });
    }
  };
})(jQuery, Drupal);
