/**
 * @file
 */

(function ($, Drupal) {
  Drupal.behaviors.tabs = {
    attach: function (context, settings) {
      $(document).ready(function () {

        $('.uw-contact-expand-all').click(function () {
          $('.uw-contact details').each(function () {
            console.log($(this));
            $(this).attr("open", "");
          });
        });

        $('.uw-contact-collapse-all').click(function () {
          $('.uw-contact details').each(function () {
            console.log($(this));
            $(this).removeAttr("open");
          });
        });

      });
    }
  };
})(jQuery, Drupal);
