/**
 *  Javascript enhancements for the header search.
 *
 * @file
 */

(function ($, document, Drupal) {
    Drupal.behaviors.wcmsheadersearchbar = {
        attach: function (context, settings) {
            $(document).ready(
                function () {
                    $(".uw-search--checkboxlabel").on(
                        "click", function () {
                            $(this).toggleClass("close");
                            $(".uw-header__masthead").toggleClass("open");
                        }
                    );

                    function debouncer(func)
                    {
                        var timeoutID,
                        timeout = 100;
                        return function () {
                            var scope = this,
                            args = arguments;
                            clearTimeout(timeoutID);
                            timeoutID = setTimeout(
                                function () {
                                    func.apply(scope, Array.prototype.slice.call(args));
                                }, timeout
                            );
                        };
                    }
                    // Check the width of the screen and.
                    function checkWidth()
                    {
                        // Set screenWidth var.
                        var screenWidth = $(window).width();
                        if (screenWidth >= 768) {
                            if ($(".uw-header__masthead").hasClass("open")) {
                                $(".uw-search--checkboxlabel").click();
                            }
                        }
                    }
                    // Listen to event resize and apply the debouncer
                    // to the menuCheckWidth function.
                    $(window).resize(
                        debouncer(
                            function () {
                                checkWidth();
                            }
                        )
                    );
                    checkWidth();
                }
            );
        }
    };
})(jQuery, document, Drupal);
