<?php

/**
 * @file
 * Function addPathFunction().
 *
 * phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidName
 */

/**
 * Twig function.
 */
function addPathFunction(\Twig_Environment &$env, $config) {
  $env->addFunction(new \Twig_SimpleFunction('path', function ($string) {
    if ($string === '<front>') {
      return '/';
    }
    else {
      return $string;
    }
  }));
}
