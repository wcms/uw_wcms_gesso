<?php

/**
 * @file
 * Function addUrlFunction().
 *
 * phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidName
 */

/**
 * Twig function.
 */
function addUrlFunction(\Twig_Environment &$env, $config) {
  // https://www.drupal.org/node/2486991
  $env->addFunction(new \Twig_SimpleFunction('url', function ($string) {
    return '#';
  }));
}
