<?php

/**
 * @file
 * Function addCreateAttributeFunction().
 *
 * phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidName
 */

/**
 * Twig function.
 */
function addCreateAttributeFunction(\Twig_Environment &$env, $config) {
  $function = new Twig_SimpleFunction(
    'create_attribute',
    function ($attributes = []) {
      foreach ($attributes as $key => $value) {
        if (!is_array($value)) {
          $value = [$value];
        }
        print ' ' . $key . '="' . implode(' ', $value) . '"';
      }
    },
    ['is_safe' => ['html']]
  );
  $env->addFunction($function);
}
