<?php

/**
 * @file
 * Function addLinkFunction().
 *
 * phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidName
 */

/**
 * Twig function.
 */
function addLinkFunction(\Twig_Environment &$env, $config) {
  $env->addFunction(new Twig_SimpleFunction(
    'link',
    function ($title, $url, $attributes) {
      if (isset($attributes) && isset($attributes['class'])) {
        $classes = implode(' ', $attributes['class']);
        return '<a href="' . $url . '" class="' . $classes . '">' . $title . '</a>';
      }
      else {
        return '<a href="' . $url . '">' . $title . '</a>';
      }
    },
    ['is_safe' => ['html']]
  ));
}
