<?php

/**
 * @file
 * Function addAttachLibraryFunction().
 *
 * phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidName
 */

/**
 * Twig function.
 */
function addAttachLibraryFunction(\Twig_Environment &$env, $config) {
  $env->addFunction(new \Twig_SimpleFunction('attach_library', function ($string) {
    return;
  }));
}
