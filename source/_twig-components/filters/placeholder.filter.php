<?php

/**
 * @file
 * Function addPlaceholderFilter().
 *
 * phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidName
 */

/**
 * Twig filter.
 */
function addPlaceholderFilter(\Twig_Environment &$env, $config) {
  $env->addFilter(new \Twig_SimpleFilter('placeholder', function ($string) {
    return $string;
  }));
}
