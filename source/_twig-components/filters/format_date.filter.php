<?php

/**
 * @file
 * Function addFormatDateFilter().
 *
 * phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidName
 */

/**
 * Twig filter.
 */
function addFormatDateFilter(\Twig_Environment &$env, $config) {
  $env->addFilter(new \Twig_SimpleFilter('format_date', function ($string) {
    return $string;
  }));
}
