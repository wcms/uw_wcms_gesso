<?php

/**
 * @file
 * Function addRenderFilter().
 *
 * phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidName
 */

/**
 * Twig filter.
 */
function addRenderFilter(\Twig_Environment &$env, $config) {
  // Drupal Render filter.
  $env->addFilter(new \Twig_SimpleFilter('render', function ($string) {
    return $string;
  }));
}
