<?php

/**
 * @file
 * Function addTFilter().
 *
 * phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidName
 */

/**
 * Twig filter.
 */
function addTFilter(\Twig_Environment &$env, $config) {
  // Drupal translate filter.
  $env->addFilter(new \Twig_SimpleFilter('t', function ($string) {
    return $string;
  }));
}
