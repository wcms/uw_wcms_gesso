<?php

/**
 * @file
 * Function addCleanIdFilter().
 *
 * phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidName
 */

/**
 * Twig filter.
 */
function addCleanIdFilter(\Twig_Environment &$env, $config) {
  $env->addFilter(new \Twig_SimpleFilter('clean_id', function ($string) {
    return $string;
  }));
}
