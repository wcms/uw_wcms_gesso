<?php

/**
 * @file
 * Function addWithoutFilter().
 *
 * phpcs:disable Drupal.NamingConventions.ValidFunctionName.InvalidName
 */

/**
 * Twig filter.
 */
function addWithoutFilter(\Twig_Environment &$env, $config) {
  $env->addFilter(new Twig_SimpleFilter('without', function ($string) {
    return $string;
  }));
}
