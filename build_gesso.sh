env_option=${1:-1}
build_option=${2:-1}
HTML_PATH=${3:-"/var/www/html"}
DRUPAL_PATH=${4:-/var/www/drupal8}
gesso_version="8.x-3.2-uw_wcms2"

export PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true

if [[ -L "$HTML_PATH/uw_wcms_gesso" ]]; then
  echo ""
  echo "*************************************************************************"
  echo "Removing old gesso symlink ..."
  rm -rf "$HTML_PATH/uw_wcms_gesso"
  echo "Done removing old gesso symlink."
  echo "**************************************************************************"
  echo ""
fi

cd "$DRUPAL_PATH/web/profiles/uw_base_profile/modules/custom/uw_wcms_gesso"

if [[ -d gesso ]]; then
  echo "**************************************************************************************"
  echo "Removing old gesso ..."
  rm -rf gesso
  echo "Done removing old gesso."
  echo "**************************************************************************************"
  echo ""
fi

git clone --branch $gesso_version https://git.uwaterloo.ca/drupal-org/gesso.git
if [ "$?" != 0 ]; then
  echo "Error: git clone failed"
  exit 2;
fi



if [ -f "gulpfile.js" ]; then
  cp gulpfile.js gesso
fi

if [ -f "package.json" ]; then
  cp package.json gesso
fi

echo "**************************************************************************************"
echo "Removing Gesso files ..."
rm gesso/gesso.layouts.yml
rm gesso/gesso.info.yml
rm gesso/gesso.libraries.yml
rm gesso/gesso.breakpoints.yml
rm gesso/Dockerfile
rm gesso/screenshot.png
rm -rf gesso/templates
echo "Done removing gesso theme files."
echo "**************************************************************************************"
echo ""

echo "**************************************************************************************"
echo "Running npm install ..."
cd gesso
npm install
if [ "$?" != 0 ]; then
  echo "Error: npm install failed"
  exit 2;
fi
echo "Done npm install."
echo "**************************************************************************************"
echo ""

echo "**************************************************************************************"
echo "Symbolic linking source directory"
if [ -d "source" ]; then
  echo ""
  echo "**************************************************************************************"
  echo "Removing old source directory ..."
  rm -rf source
  echo "Done removing old source directory."
  echo "**************************************************************************************"
  echo ""
fi
ln -fs "../source" source
echo "Done symbolic linking source directory."
echo "**************************************************************************************"
echo ""

echo "**************************************************************************************"
echo "Removing style lint config and replacing with UW config ..."
rm .stylelintrc.yml
cp ../.stylelintrc.yml .
echo "Done removing style lint config and replacing with UW config."
echo "**************************************************************************************"
echo ""

echo "**************************************************************************************"
echo "Copying patched files ..."
echo "Done copying patched files."
echo "**************************************************************************************"
echo ""
cp ../patched_files/* node_modules/@basalt/twig-renderer/vendor/twig/twig/lib/Twig/
echo "**************************************************************************************"

if [[ $build_option == 1 || $build_option == 3 ]]; then
  echo "Running gulp build ..."
  gulp build
  if [ "$?" != 0 ]; then
    echo "Error: gulp build failed"
    exit 2;
  fi
  cd ..
  echo "Done running gulp build."
  echo "**************************************************************************************"
  echo ""

  echo "**************************************************************************************"
  echo "Creating symlink for gesso ..."
  cd $HTML_PATH
  if [[ $env_option == 1 ]]; then
    ln -s "$DRUPAL_PATH"/web/profiles/uw_base_profile/modules/custom/uw_wcms_gesso uw_wcms_gesso
  elif [[ $env_option == 2 ]]; then
    cd /var/www/html/web
    ln -s ./profiles/uw_base_profile/modules/custom/uw_wcms_gesso uw_wcms_gesso
    cd -
  else
    ln -s ../web/profiles/uw_base_profile/modules/custom/uw_wcms_gesso uw_wcms_gesso
  fi
  echo "Done creating symlink for gesso."
  echo "**************************************************************************************"
  echo ""

else

  echo "**************************************************************************************"
  echo "Compiling CSS and JS ..."
  gulp build
  cd ..
  rm css/styles.css
  rm css/pattern-lab.css
  rm css/uw-dashboards.css
  echo "Done compiling CSS and JS"
  echo "**************************************************************************************"
  echo ""

  echo "**************************************************************************************"
  echo "Setting up gesso helper ..."
  mv gesso/gesso_helper .
  rm -rf gesso/*
  mv gesso_helper gesso
  echo "Done setting up gesso helper"
  echo "**************************************************************************************"
fi

unset PUPPETEER_SKIP_CHROMIUM_DOWNLOAD
